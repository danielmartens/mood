date - Timestamp in ms
activityType - user activity recognized by Google Activity Recognition API
activityConfidence - confidence of the activity classifier
ambientLightLevel - Light Level in Lux
calendarEntryId - ID of the currently ongoing calender entry
calendarName - calendar name of the currently ongoing calender entry
callType - type of call (incoming, outgoing, missed)
callPhoneNumber - hashed phone number of call partner (who called or was called)
messageAddress - hashed phone number of SMS partner (who sent or received SMS)
messageFolder - folder of the SMS (inbox, outbox, draft)
messageLength - character length of message
cellIDHash - hashed cell ID and location area code
connectivity - connectivity state  (wifi, mobile, none)
displayState - display activity (0 = not active, 1 = active)
notificationPackage - package name of the app that sent a notification that was received
notificationAction - how did the user react to the notification
heartRate - heart rate measured by the Moto360 smartwatch
statusDelay - delay in ms between mood questionnaire prompt and answer by the user
statusReason - reason why the user answered the mood questionnaire (time, connectivity_lost, independent)
statusMoodE - energetic arousal dimension of the mood accessed via the Multidimensional Mood Questionnaire (MDMQ)
statusMoodV - valence dimension of the mood accessed via the Multidimensional Mood Questionnaire (MDMQ)
statusMoodC - calmness dimension of the mood accessed via the Multidimensional Mood Questionnaire (MDMQ)

probandX.csv = full data of a subject with ID X
probandX_wocheY.csv = data collected for week nr. Y of a subject with ID X